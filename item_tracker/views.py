import datetime
import hashlib

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from item_tracker.models import Cart, Item
from item_tracker.serializers import CartSerializer, ItemSerializer


def generate_unique_id():
    current_time_utc_utf8 = datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S%f').encode('utf-8')
    return hashlib.sha256(current_time_utc_utf8).hexdigest()


class CartList(APIView):

    def get(self, request, format=None):
        carts = Cart.objects.all()
        serializer = CartSerializer(carts, many=True)
        return Response(serializer.data)


class ItemList(APIView):
    COOKIE_CART_ID_NAME = 'cart_id'
    COOKIE_CART_ID_MAX_AGE = 3600 * 72

    def get(self, request, format=None):
        items = Item.objects.all()
        serializer = ItemSerializer(items, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        cart_id = request.COOKIES.get(self.COOKIE_CART_ID_NAME, generate_unique_id())

        self._create_cart(cart_id)

        if self._create_item(request.data, cart_id):
            res_status = status.HTTP_204_NO_CONTENT
        else:
            res_status = status.HTTP_400_BAD_REQUEST

        return self._create_response_with_cookie(request.COOKIES, cart_id, res_status)

    def _create_cart(self, cart_id):
        if not Cart.objects.filter(id=cart_id).exists():
            Cart.objects.create(id=cart_id)

    def _create_item(self, data, cart_id):
        data['cart'] = cart_id
        serializer = ItemSerializer(data=data)

        is_valid = serializer.is_valid()
        if is_valid:
            if Item.objects.filter(external_id=data['external_id'], cart=cart_id).exists():
                item = Item.objects.get(external_id=data['external_id'], cart=cart_id)
                item.name = data.get('name', None)
                item.value = data.get('value', None)
                item.save()
            else:
                serializer.save()

        return is_valid

    def _create_response_with_cookie(self, cookies, cart_id, status):
        response = Response(status=status)

        if self.COOKIE_CART_ID_NAME not in cookies:
            response.set_cookie(self.COOKIE_CART_ID_NAME, cart_id, max_age=self.COOKIE_CART_ID_MAX_AGE)

        return response
