from rest_framework import serializers
from item_tracker.models import Cart, Item


class CartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cart
        fields = (
            'id',
        )
        read_only_fields = (
            'id',
        )


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = (
            'external_id',
            'name',
            'value',
            'cart',
        )
