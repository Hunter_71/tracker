from django.db import models


class Cart(models.Model):
    id = models.CharField(max_length=255, primary_key=True, blank=False, null=False)


class Item(models.Model):
    external_id = models.CharField(max_length=255)
    name = models.CharField(max_length=255, null=True)
    value = models.IntegerField(null=True)
    cart = models.ForeignKey(Cart, on_delete=models.PROTECT)
