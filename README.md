# Tracker
Author:&ensp;&ensp;&ensp;[Tomasz Zięba][1] <tomasz.zieba.71@gmail.com>

## Description
Microservice for very basic e-commerce data tracking mechanism.  
The application simply collects the data about items which the user adds to his card.

## Application configuration

### Settings module properties configuration
Since django **settings.py** module is prepared to consider some settings from local settings file,
please create one called **localsettings.py** inside `tracer` project, example below:

```python
# tracker/tracker/localsettings.py
DEBUG = False
ALLOWED_HOSTS = [MY.ALLEWED.HOST.IP]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'read_default_file': 'path/to/config/file.cnf'
        },
    }
}
```
Inside **settings.py** it would be consider if exists **localsettings.py** module, else default data
would be taken (example below).
```python
try:
    from tracker import localsettings

    DEBUG = localsettings.DEBUG
except ImportError:
    DEBUG = False
```

### Database configuration
Inside Your **mysql** or **postgresql** create dedicated database (example **tracker**).
```sql
CREATE DATABASE tracker;
``` 
Then create dedicated user for application (example **tracker**) with his own unique password and
grant all privileges to created earlier database for this user.
```sql
GRANT ALL PRIVILEGES ON tracker.* TO 'tracker'@'%' IDENTIFIED BY '******';
```

Then open database configuration file (if you head one) or update Your database connection parameters.
In database configuration file:
```ini
[client]
database = tracker
user = tracker
password = ******
host = localhost
port = 3306
default-character-set = utf8
```
In **localsettings.py**:
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'tracker',
        'USER': 'tracker',
        'PASSWORD': '*****',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}
```

### Database tables creation
To create required tables in database simply run below commands:
```bash
:~$ cd /path/to/tracker
:/path/to/tracker$ ls
item_tracker  manage.py  README.md  tracker

:/path/to/tracker$ python3 manage.py migrate
:/path/to/tracker$ python3 manage.py makemigrations item_tracker
:/path/to/tracker$ python3 manage.py migrate item_tracker
```

## Run application server

To run application server simply open terminal, open project highest level directory and run below command.
```bash
:~$ cd /path/to/tracker
:/path/to/tracker$ ls
item_tracker  manage.py  README.md  tracker

:/path/to/tracker$ python3 manage.py runserver
Performing system checks...

System check identified no issues (0 silenced).
January 30, 2019 - 09:01:11
Django version 2.1.5, using settings 'tracker.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

**Warning!**  
Applications runs with **runserver** command on localhost on default port 8000.
Applications runs until terminal is open and command would not be broken.

## Example usage
### Register item without open session
To register item without open session simply send POST request to http://localhost:8000/items/  
**curl** example below:
```bash
curl --header "Content-Type: application/json" \
     --request POST \
     --data '{"external_id":"my_external_id_1","name":"name","value":0}' \
     http://localhost:8000/items/
```
### Register item without open session and save cookies
To register item without open session and receive session cookies to continue communication inside current session
simply send extended POST request to http://localhost:8000/items/  
**curl** example below:
```bash
curl -c cookie_file.txt
     --header "Content-Type: application/json" \
     --request POST \
     --data '{"external_id":"my_external_id_1","name":"name","value":0}' \
     http://localhost:8000/items/
```
### Register item with open session
To register item with open session simply send extended POST request to http://localhost:8000/items/  
**curl** example below:
```bash
curl -b cookie_file.txt
     --header "Content-Type: application/json" \
     --request POST \
     --data '{"external_id":"my_external_id_1","name":"name","value":0}' \
     http://localhost:8000/items/
```

[1]: https://stackoverflow.com/users/3081328/hunter-71